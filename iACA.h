#include <NVX/NVX.h>
#include <Spinster/Spinster.h>
#include <ARKE/ARKE.h>
using namespace nvx;
using namespace arke;

typedef NI2<24,8,0> Num;

namespace ACA {
    class HDI;
    class Frame;
    class Attractor;
    class History;
    class HistoryNode;
    class Goal;
    class Planner;
    class Axis;
    class State;
};
