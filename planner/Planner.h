#ifndef ACA_PLANNER_H
#define ACA_PLANNER_H
#include "Goal.h"

namespace ACA {
    class Planner {
    public:
        
        Goal* plan(const State& fromState, const State& toState);
        void plan(Goal *goal);
        
        void addGoal(Goal *goal);
        void removeGoal(Goal *goal);
        
        State evaluate(const State& currentState);
        
        std::vector<Goal*> goals;
    protected:
        
    };
}

#endif
