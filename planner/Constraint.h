#ifndef ACA_CONSTRAINT_H
#define ACA_CONSTRAINT_H
#include "State.h"

namespace ACA {
    class Constraint : public Goal {
    public:
        virtual ~Constraint();
        
        virtual double priority(const State& current) override;
        virtual void priority(double min, double max, bool inv = false);
    protected:
        double minPriority, maxPriority;
        bool inverted;
        State lower, upper, weight;
    };
}

#endif
