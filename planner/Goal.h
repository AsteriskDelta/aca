#ifndef ACA_GOAL_H
#define ACA_GOAL_H
#include "Frame.h"

namespace ACA {
    class Goal {
    public:
        Goal();
        virtual ~Goal();
        
        virtual double priority(const State& current);
        virtual double deviation();
        
        virtual Goal* current();
        
        struct {
            Frame begin, end;
        } state;
        
        inline bool compound() const {
            return !subgoals.empty();
        }
    protected:
        std::vector<Goal*> subgoals;
        int currentGoal;
        
        double priority_;
    }
}

#endif
