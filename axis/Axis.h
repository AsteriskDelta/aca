#ifndef ACA_AXIS_H
#define ACA_AXIS_H
#include <ARKE/Identified.h>
#include <ARKE/Named.h>
#include "History.h"

namespace ACA {
    class Axis;
    class Axis : public Identified<Axis*>, public Named, public History {
    public:
        Axis();
        virtual ~Axis();
        
        Num min, max, increment;
        
        typedef uint8_t Type;
        static constexpr const Type
            Invalid = 0,
            Input   = 1,
            Output  = 2,
            Duplex  = 3;
        Type type;
        
        inline bool isInput() const {
            return type == Input || type == Duplex;
        }
        inline bool isOutput() const {
            return type == Output || type == Duplex;
        }
    protected:
        
    }
}

#endif
