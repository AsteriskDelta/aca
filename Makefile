NAME = ACA

#Directory that contains .cpp files with main()
EXEC_DIRS = ./specs/

#directories with classes/files to be compiled into objects
SRC_DIRS = 

#directories containing headers that need to be installed
HEADER_DIRS = axis hdi lib planner api
HEADER_CP_DIRS = 
HEADER_FILES += iACA.h ACA.h
#Header install subdirectory (ie, in /usr/include: defaults to $(SYS_NAME))
HEADERS_OUT_DIR = ACA/

#Choose ONE header, if any, to precompile and cache (not for developement!!!)
PCH =

#Default platform
TARGET_PLATFORM ?= Desktop
#Local build output directory
BUILD_DIR = build

#Compiler
CXX ?= g++
#CFLAGS (appended to required ones)
CXXPLUS += -fno-stack-protector --include iACA.h
#SYS_FLAGS (prefix and possible override system CFLAGS, may break things)
CC_SYS_FLAGS ?= 
#Optimization flags, supporting PGO if needed
OPTI ?= -march=native -mtune=native -O0
#-Ofast -flto
#Include paths, ie -I/path/to/headers/
INCPATH += 
#Libraries, ie -lopenmp
LIBS += 

include /usr/include/LPBT.mk

#Warnings, ie -Wall
WARNINGS +=

install::

uninstall::

autorun::

disable::


