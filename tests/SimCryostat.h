class SimCryostation {
public:
    
    double platformTemp() const;
    double stage2Temp() const;
    double stage1Temp() const;
    
    bool setCompressorSpeed(double fract);
    double getCompressorSpeed() const;
    
    bool getVentValve() const;
    void setVentValve(bool st);
    
    double getVacuumSpeed() const;
    double setVacuumSpeed(double fract);
    
    double getPressure() const;
    
    void update(double deltaTime);
protected:
    struct {
        double platform, stage1, stage2;
    } temp;
    double pressure;
    
    double compressorSpeed, vacuumSpeed;
    
    struct {
        bool vent;
    } valve;
}
