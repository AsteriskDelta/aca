double SimCryostation::platformTemp() const {
    return temp.platform;
}
double SimCryostation::stage2Temp() const {
    return temp.stage2;
}
double SimCryostation::stage1Temp() const {
    return temp.stage1;
}

bool SimCryostation::setCompressorSpeed(double fract) {
    return vacuumSpeed;
}
double SimCryostation::getCompressorSpeed() const {
    return compressorSpeed;
}

bool SimCryostation::getVentValve() const {
    return valve.vent;
}
void SimCryostation::setVentValve(bool st) {
    valve.vent = st;
}

double SimCryostation::getVacuumSpeed() const {
    return vacuumSpeed;
}
double SimCryostation::setVacuumSpeed(double fract) {
    vacuumSpeed = fract;
}

double SimCryostation::getPressure() const {
    return pressure;
}

void SimCryostation::update(double deltaTime) {
    const double radiationNoise = 0.02;
}
